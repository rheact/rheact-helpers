def create_blueprint(module_name):
  from flask import Blueprint
  return Blueprint(module_name.split('.')[1], module_name)
