"""
    rheact.core.cache
    ~~~~~~~~~~~~~~~~
    Implements Google App Engine Memcached Service.
"""

import os
import yaml
import pickle

from google.appengine.api import memcache


def write(key, value, token=None):
    """Write a key/value pair to Google App Engine Memcached.
    If a token is specified, the key prefix will be token:<token>.
    """
    if value is not None:
        if token:
            memcache.set("token:%s:%s" % (token, key), value)
        else:
            memcache.set(key, value)

def read(key, token=None):
    """Read a value from Google App Engine Memcached.
    If a token is specified, the key prefix will be token:<token>.
    """
    if token:
        result = memcache.get("token:%s:%s" % (token, key))
    else:
        result = memcache.get(key)
    return result
