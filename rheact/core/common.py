"""
    rheact.core.common
    ~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements common Python helpers.
"""


def to_int(arg):
    if arg is None: return None    
    try:
        return int(arg)
    except ValueError:
        return None


def get_key(dict, key):    
    try:
        if key in dict:
            return dict[key]
        else:
            return None
    except KeyError:
        return None
