from google.appengine.api import urlfetch
import json
import urllib


CONTENT_TYPES_HEADER = {
  'urlencode': 'application/x-www-form-urlencoded',
  'json': 'application/json'
}
METHODS_ALLOW_PAYLOAD = ['POST', 'PATCH', 'PUT']
URLFETCH_METHODS = {
  'POST': urlfetch.POST,
  'GET': urlfetch.GET,
  'PATCH': urlfetch.PATCH,
  'PUT': urlfetch.PUT,
  'DELETE': urlfetch.DELETE
}


def execute_request(method, endpoint, post_data, headers={}, request_type='urlencode', response_type='json'):
  # prepare the post data according the request type and method
  if METHODS_ALLOW_PAYLOAD:    
    post_data = _get_post_data_according_request_type(post_data, request_type)
  else:
    post_data = _build_querystring(post_data)
    endpoint = endpoint + '?' + post_data

  # prepare content type header
  headers = _set_headers_according_request_type(headers, request_type)
  
  # set the deadline as the maximum allowed
  urlfetch.set_default_fetch_deadline(60)

  # get the urlfetch parameter
  urlfetch_method = URLFETCH_METHODS[method]

  # build the parameters which will be sent to urlfetch
  params = {
    'method': urlfetch_method,
    'headers': headers,
    'url': endpoint
  }
  # if the method is POST, PUT or PATCH add the request body
  if method in METHODS_ALLOW_PAYLOAD: params['payload'] = post_data 
  
  # execute the request
  response = urlfetch.fetch(**params)  

  # parse the body according the response type
  body = _get_body_according_response_type(response.content, response_type)  

  # envelope the response
  result = {
    'body': body,
    'status_code': response.status_code
  }
  return result


def execute_post(endpoint, post_data, headers={}, request_type='urlencode', response_type='json'):
  return execute_request('POST', endpoint, post_data, headers, request_type, response_type)


def execute_patch(endpoint, post_data, headers={}, request_type='urlencode', response_type='json'):
  return execute_request('PATCH', endpoint, post_data, headers, request_type, response_type)


def execute_get(endpoint, parameters={}, headers={}, response_type='json'):
  return execute_request('GET', endpoint, parameters, headers, 'urlencode', response_type)


def execute_delete(endpoint, parameters={}, headers={}, response_type='json'):
  return execute_request('DELETE', endpoint, parameters, headers, 'urlencode', response_type)


def _build_querystring(parameters):
  qs = ''
  if len(parameters) > 0:
    qs = '?'
  for p in parameters:    
    value = ''
    t = type(parameters[p])

    if t is str or t is unicode:
      value = parameters[p]
    elif t is int:
      value = str(parameters[p])
    elif t is bool:
      value = 'true' if parameters[p] else 'false'
    elif t is list:
      value = None

    # value = urllib.quote(value.encode('utf8'))
    if value is not None:
      value = urllib.quote(value)

    if t is not list:
      qs += p + '=' + value + '&'
    else:
      value = [str(i) for i in parameters[p]]
      for i in value:
        x = p + '[]'
        qs += x + '=' + i + '&'
    

  return qs[:-1]


def _set_headers_according_request_type(headers, request_type):
  if request_type in CONTENT_TYPES_HEADER:
    headers['Content-Type'] = CONTENT_TYPES_HEADER[request_type]
  return headers


def _get_post_data_according_request_type(post_data, request_type):
  if request_type == 'urlencode':
    str_post_data = {}
    for k, v in post_data.iteritems():
      str_post_data[k] = unicode(v).encode('utf-8')
    return urllib.urlencode(str_post_data)

  if request_type == 'json':
    return json.dumps(post_data)


def _get_body_according_response_type(body, response_type):
  if response_type == 'json':
    try:
      return json.loads(body)
    except:
      return body
  if response_type == 'raw':
    return body