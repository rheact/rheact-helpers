from flask import request


def get_json_data(raise_error=True):
  import json
  from flask import abort

  try:
    data = json.loads(request.data)
    if not data:
      if raise_error:
        abort(400, 'The JSON cannot be empty')
      else:
        return None
    return data
  except ValueError:
    if raise_error:
      abort(400, 'Invalid JSON object')
    else:
      return None


def get_args_data(key, default_value=None):
  if key in request.args:
    return request.args[key]
  else:
    return default_value


def get_form_data(key, default_value=None):
  if key in request.form:
    return request.form[key]
  else:
    return default_value  