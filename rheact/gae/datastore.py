from __future__ import absolute_import
from google.appengine.ext import ndb
from google.appengine.api import search
from google.appengine.ext import db
from google.appengine.ext.db import BadRequestError
from google.net.proto.ProtocolBuffer import ProtocolBufferDecodeError
from rheact.shared.ndb_json_helpers import entity_to_dict
import logging


DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


class ModelMetaclass(ndb.MetaModel):
    def __new__(cls, name, bases, attrs):
        return super(ModelMetaclass, cls).__new__(cls, name, bases, attrs)


class ModelBase(ndb.Model):
    __metaclass__ = ModelMetaclass
    created_on = ndb.DateTimeProperty(auto_now_add=True)
    updated_on = ndb.DateTimeProperty(auto_now=True)

    MANDATORY_FIELDS = []
    INSERTABLE_FIELDS = []
    DEFAULT_DATE_FIELDS = ['created_on', 'updated_on']
    GAE_DEFAULT_FIELDS = ['id', 'app', 'namespace', 'parent', 'key']
    DATE_FIELDS = []
    INDEXABLE = False
    NON_EXPORTABLE = []
    UNIQUENESS = []
    errors = {}
    valid = True

    def _post_put_hook(self, future):
        if self.INDEXABLE:
            fields = self.to_index()
            facets = self.get_facets()

            try:
                doc = search.Document(doc_id=str(
                    self.key.id()), fields=fields, facets=facets)
                search.Index(self.INDEX_NAME).put(doc)
            except search.Error:
                logging.exception('Put failed')

    @classmethod
    def _post_delete_hook(cls, key, future):
        if cls.INDEXABLE:
            try:
                search.Index(cls.INDEX_NAME).delete(str(key.id()))
            except search.Error:
                logging.exception('Delete failed')

    def to_dict(self):
        result = entity_to_dict(self)
        return result

    def to_json(self, includes=None, excludes=None):
        if excludes is None:
            excludes = []

        self.DATE_FIELDS = self.DATE_FIELDS + self.DEFAULT_DATE_FIELDS
        excludes = excludes + self.DATE_FIELDS + self.NON_EXPORTABLE

        struct_properties = [p for p in self.__get_all_structured_properties() if p != 'details']
        excludes = excludes + struct_properties

        result = entity_to_dict(self, includes, excludes)
        result = self.convert_geo_fields(result)
        result = self.convert_date_fields(result)

        return result

    def to_gae_json(self):
        return self.to_json(excludes=self.GAE_DEFAULT_FIELDS)

    def convert_geo_fields(self, result):
        if hasattr(self, 'GEO_FIELDS'):
            if self.GEO_FIELDS != []:
                for field in self.GEO_FIELDS:
                    t = type(getattr(self, field))
                    if t is db.GeoPt:
                        v = getattr(self, field)
                        result[field] = {
                            'lat': v.lat,
                            'lon': v.lon
                        }
        return result

    def convert_date_fields(self, result):
        import datetime
        if hasattr(self, 'DATE_FIELDS'):
            if self.DATE_FIELDS != []:
                for field in self.DATE_FIELDS:
                    t = type(getattr(self, field))
                    if t is datetime.datetime:
                        date_before = getattr(self, field)
                        date_after = date_before.strftime(DATE_FORMAT)
                        result[field] = date_after
                    elif t is list:
                        dates_before = getattr(self, field)
                        dates_after = [d.strftime(DATE_FORMAT)
                                       for d in dates_before]
                        result[field] = dates_after
                    else:
                        v = getattr(self, field)
                        if v is not None:
                            logging.debug(
                                'type %s not allowed of the field %s' % (t, field))

        return result

    def build(self, params):
        import pprint
        if hasattr(self, 'INSERTABLE_FIELDS'):
            for key in self.INSERTABLE_FIELDS:
                if key in self.to_dict() and key in params.keys():
                    setattr(self, key, params[key])

        if hasattr(self, 'GEO_FIELDS'):
            for key in self.GEO_FIELDS:
                if key in self.to_dict() and key in params.keys():
                    geofield = db.GeoPt(lat=params[key]['lat'], lon=params[key]['lon'])
                    setattr(self, key, geofield)

        struct_properties = self.__get_all_structured_properties()
        for key in struct_properties:
            if key in self.to_dict() and key in params.keys():
                if type(params[key]) is not dict:
                    pprint.pprint(params[key].to_gae_json())
                    setattr(self, key, params[key])
                else:
                    if key == 'details':
                        detail = ndb.Expando()
                        for d in params[key]:
                            setattr(detail, d, params[key][d])
                        setattr(self, key, detail)

                    pprint.pprint('key: %s' % key)

    def __get_all_structured_properties(self):
        from google.appengine.ext.ndb.model import StructuredProperty
        return [p for p in self._properties if type(self._properties[p]) == StructuredProperty]

    def validate_mandatory_fields(model):
        missing = []

        if hasattr(model, 'MANDATORY_FIELDS'):
            for key in model.MANDATORY_FIELDS:
                value = getattr(model, key)
                if not value:
                    missing.append(key)

        return missing

    def validate_unique_fields(model):
        errors = []

        if hasattr(model, 'UNIQUENESS'):
            for key in model.UNIQUENESS:
                value = getattr(model, key)

                query = model.query(ndb.GenericProperty(key) == value)
                rows = query.fetch()

                if len(rows) > 0:
                    errors.append(key)

        return errors

    def put(self):
        self.errors = {}
        missing = self.validate_mandatory_fields()
        if len(missing) > 0:
            self.errors['missing'] = missing

        uniqueness = self.validate_unique_fields()
        if len(uniqueness) > 0:
            self.errors['uniqueness'] = uniqueness

        self.valid = len(self.errors) == 0
        if self.valid:
            super(ModelBase, self).put()
            return True
        else:
            return False

    def error_messages(self):
        if len(self.errors) > 0:
            error_response = {"message": "Invalid request."}
            if 'missing' in self.errors:
                error_response['missing'] = self.errors['missing']
            if 'uniqueness' in self.errors:
                error_response['uniqueness'] = self.errors['uniqueness']
            return error_response
        else:
            return []

    @classmethod
    def delete(self, id):
        ndb.Key(self, int(id)).delete()

    @classmethod
    def all(self, order=None, keys_only=False):
        return self.query().fetch(keys_only=keys_only)

    @classmethod
    def find_by_key(self, token):
        q = None
        try:
            key = ndb.Key(urlsafe=token)
            q = key.get()
        except ProtocolBufferDecodeError:
            q = None
        except BadRequestError:
            q = None
        return q

    @classmethod
    def create(self, **params):
        obj = self(**params)
        obj.put()
        return obj

    @classmethod
    def delete_all(self):
        [self.delete(o.key.id()) for o in self.all()]
        return True

    @classmethod
    def find(self, id):
        return self.get_by_id(int(id))


def fetch_data(query, args):
    limit = 20
    if 'limit' in args:
        limit = int(args['limit'])

    offset = 0
    if 'offset' in args:
        offset = int(args['offset'])

    return query.fetch(limit, offset=offset)
