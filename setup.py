# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='rheact-helpers',
    version='1.0.0',
    description='Rheact Helpers',
    long_description=long_description,
    url='https://bitbucket.org/rheact/rheact-helpers',
    author='Armando Miani',
    author_email='armando.miani@gmail.com',
    license='MIT',
    classifiers=[
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='common helpers http-handlers api',
    packages=[
        'rheact', 
        'rheact.core',
        'rheact.gae',
        'rheact.shared'],
    install_requires=['flask', 'Flask-Cors']
)